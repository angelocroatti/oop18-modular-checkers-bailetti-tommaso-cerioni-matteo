package it.unibo.modularcheckers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;

import it.unibo.modularcheckers.checkers.model.engine.CheckersStepTreeCalculator;
import it.unibo.modularcheckers.checkers.model.engine.CheckersStepTreeFilter;
import it.unibo.modularcheckers.checkers.model.piece.King;
import it.unibo.modularcheckers.checkers.model.piece.Man;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.ChessboardImpl;
import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.engine.StepTreeCalculator;
import it.unibo.modularcheckers.model.engine.StepTreeFilter;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.StepImpl;
import it.unibo.modularcheckers.model.move.StepTreeBuilderImpl;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.move.TreeBuilder;
import it.unibo.modularcheckers.model.move.TreeImpl;

/**
 * Basic class test for StepTreeFilter. CHECKSTYLE: MagicNumber OFF
 */
public class StepTreeFilterTest {

    private final StepTreeCalculator treeCalculator = new CheckersStepTreeCalculator(Color.BLACK);
    private final StepTreeFilter treeFilter = new CheckersStepTreeFilter();
    private final TreeBuilder<Step> builder = new StepTreeBuilderImpl();

    /**
     * Test the map that doesn't need to be filtered.
     */
    @Test
    public void testNoChange() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        board.getBlock(new Coordinate(4, 0)).setPiece(new Man(Color.WHITE));
        assertEquals("No changes between the builded and the filtered", unfilteredMap(board), filteredMap(board));
    }

    /**
     * Test the map that has an eat step and a normal step.
     */
    @Test
    public void testDeleteSimpleEat() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        assertNotEquals("No changes between the builded and the filtered", unfilteredMap(board), filteredMap(board));
    }

    /**
     * Test the king has not priority over the Man.
     */
    @Test
    public void testKingFilterPriority() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(4, 0)).setPiece(new King(Color.BLACK));
        board.getBlock(new Coordinate(5, 7)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(0, 0)).setPiece(new Man(Color.WHITE));
        assertEquals("Must be no changes between the builded and the filtered", unfilteredMap(board),
                filteredMap(board));
    }

    /**
     * Test if the King can only eat where there are more pieces.
     */
    @Test
    public void testManConsecutiveEat() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new Man(Color.BLACK));
        assertNotEquals("One Step is deleted.", unfilteredMap(board), filteredMap(board));
    }

    /**
     * Test if the King can only eat where there are more pieces.
     */
    @Test
    public void testManConsecutiveEatPriority() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(1, 1)).setPiece(new Man(Color.BLACK));
        builder.startNewTree(new StepImpl(new Coordinate(2, 0)));
        builder.addValue(new StepImpl(new Coordinate(4, 2), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(6, 4), new Pair<>(new Coordinate(5, 3), new Man(Color.BLACK))));
        final Tree<Step> expectedTree = builder.returnTree();
        assertNotEquals("One Step is deleted.", unfilteredMap(board), filteredMap(board));
        assertEquals("The expected tree and the filtered are different", expectedTree,
                filteredMap(board).values().stream().findFirst().get());
    }

    /**
     * Test if the King can only eat where there are more pieces.
     */
    @Test
    public void testKingConsecutiveEatPriority() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 0)).setPiece(new King(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(1, 1)).setPiece(new Man(Color.BLACK));
        builder.startNewTree(new StepImpl(new Coordinate(2, 0)));
        builder.addValue(new StepImpl(new Coordinate(4, 2), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(6, 4), new Pair<>(new Coordinate(5, 3), new Man(Color.BLACK))));
        final Tree<Step> expectedTree = builder.returnTree();
        assertNotEquals("One Step is deleted.", unfilteredMap(board), filteredMap(board));
        assertEquals("The expected tree and the filtered one are different", expectedTree,
                filteredMap(board).values().stream().findFirst().get());

    }

    /**
     * Test the king eating 4 pieces in circular way.
     */
    @Test
    public void testKingCircularEat() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(2, 2)).setPiece(new King(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(3, 3)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new Man(Color.BLACK));
        builder.startNewTree(
                new StepImpl(new Coordinate(4, 4), new Pair<>(new Coordinate(3, 3), new Man(Color.BLACK))));
        builder.addValue(new StepImpl(new Coordinate(6, 2), new Pair<>(new Coordinate(5, 3), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(4, 0), new Pair<>(new Coordinate(5, 1), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(2, 2), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        final Tree<Step> firstEat = builder.returnTree();
        builder.startNewTree(
                new StepImpl(new Coordinate(4, 0), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        builder.addValue(new StepImpl(new Coordinate(6, 2), new Pair<>(new Coordinate(5, 1), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(4, 4), new Pair<>(new Coordinate(5, 3), new Man(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(2, 2), new Pair<>(new Coordinate(3, 3), new Man(Color.BLACK))));
        final Tree<Step> secondEat = builder.returnTree();
        final Tree<Step> completeTree = new TreeImpl<Step>(new StepImpl(new Coordinate(2, 2)),
                Arrays.asList(firstEat, secondEat));

        assertEquals("The filtered map is not like the expected.", completeTree,
                filteredMap(board).values().stream().findFirst().get());
    }

    /**
     * Test if the King eats where there are more Kings.
     */
    @Test
    public void testKingEatWhereMoreKingsAreEaten() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(4, 0)).setPiece(new King(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new King(Color.BLACK));
        board.getBlock(new Coordinate(1, 3)).setPiece(new King(Color.BLACK));
        board.getBlock(new Coordinate(5, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new King(Color.BLACK));
        builder.startNewTree(new StepImpl(new Coordinate(4, 0)));
        builder.addValue(new StepImpl(new Coordinate(2, 2), new Pair<>(new Coordinate(3, 1), new King(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(0, 4), new Pair<>(new Coordinate(1, 3), new King(Color.BLACK))));
        final Tree<Step> completeTree = builder.returnTree();
        assertNotEquals("A branch is deleted", unfilteredMap(board), filteredMap(board));
        assertEquals("The expected tree and the filtered one are different", completeTree,
                filteredMap(board).values().stream().findFirst().get());
    }

    /**
     * Test if the best move is when the king appears first in the Tree.
     */
    @Test
    public void testKingEatWhenKingIsFirst() {
        final Chessboard board = new ChessboardImpl();
        board.getBlock(new Coordinate(4, 0)).setPiece(new King(Color.WHITE));
        board.getBlock(new Coordinate(3, 1)).setPiece(new King(Color.BLACK));
        board.getBlock(new Coordinate(1, 3)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 1)).setPiece(new Man(Color.BLACK));
        board.getBlock(new Coordinate(5, 3)).setPiece(new King(Color.BLACK));
        builder.startNewTree(new StepImpl(new Coordinate(4, 0)));
        builder.addValue(new StepImpl(new Coordinate(2, 2), new Pair<>(new Coordinate(3, 1), new King(Color.BLACK))));
        builder.goToNextChild();
        builder.addValue(new StepImpl(new Coordinate(0, 4), new Pair<>(new Coordinate(1, 3), new Man(Color.BLACK))));
        final Tree<Step> exactTree = builder.returnTree();
        assertNotEquals(unfilteredMap(board), filteredMap(board));
        assertEquals("The expected tree and the filtered one are different", exactTree,
                filteredMap(board).values().stream().findFirst().get());
    }

    /////////////////////
    /****** UTIL. ******/
    /////////////////////

    private Map<Coordinate, Tree<Step>> unfilteredMap(final Chessboard board) {
        return board.getBlocks().keySet().stream().filter(c -> board.getBlock(c).pieceExists())
                .filter(c -> board.getBlock(c).getPiece().get().getColor().equals(Color.WHITE))
                .collect(Collectors.toMap(c -> c, c -> treeCalculator.calc(board, c)));
    }

    private Map<Coordinate, Tree<Step>> filteredMap(final Chessboard board) {
        return treeFilter.filter(unfilteredMap(board), board, Color.BLACK);
    }
}
