package it.unibo.modularcheckers;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import it.unibo.modularcheckers.checkers.model.engine.CheckersStepTreeCalculator;
import it.unibo.modularcheckers.checkers.model.piece.King;
import it.unibo.modularcheckers.checkers.model.piece.Man;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.ChessboardImpl;
import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.engine.StepTreeCalculator;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.StepImpl;
import it.unibo.modularcheckers.model.move.StepTreeBuilderImpl;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.move.TreeBuilder;

/** Test for StepTreeBuilder. **/
public class StepTreeBuilderTest {

    private final StepTreeCalculator calculator = new CheckersStepTreeCalculator(Color.BLACK);
    private final TreeBuilder<Step> treeBuilder = new StepTreeBuilderImpl();

    // CHECKSTYLE: MagicNumber OFF

    /** Test the single man basic movements. */
    @Test
    public void testSingleManForward() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(2, 0));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(2, 0)));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 1)));
        treeBuilder.addValue(new StepImpl(new Coordinate(3, 1)));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white Man normal movements.", resultingTree, actualTree);
        exampleBoard.getBlock(new Coordinate(1, 7)).setPiece(new Man(Color.BLACK));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(1, 7));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(1, 7)));
        treeBuilder.addValue(new StepImpl(new Coordinate(0, 6)));
        treeBuilder.addValue(new StepImpl(new Coordinate(2, 6)));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing black Man normal movements.", resultingTree, actualTree);
    }

    /**
     * Test the single man basic movements, but pieces are in the corners of the
     * board.
     */
    @Test
    public void testSingleManForwardBlocked() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(0, 0)).setPiece(new Man(Color.WHITE));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(0, 0));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(0, 0)));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 1)));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white Man normal movements in the corner.", resultingTree, actualTree);
        exampleBoard.getBlock(new Coordinate(7, 7)).setPiece(new Man(Color.BLACK));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(7, 7));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(7, 7)));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 6)));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing black Man normal movements in the corner.", resultingTree, actualTree);
    }

    /** Test the single King basic movements. */
    @Test
    public void testSimpleKingMovements() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(3, 1)).setPiece(new King(Color.WHITE));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(3, 1));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(3, 1)));
        treeBuilder.addValue(new StepImpl(new Coordinate(2, 2)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 2)));
        treeBuilder.addValue(new StepImpl(new Coordinate(2, 0)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 0)));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white King normal movements.", resultingTree, actualTree);
        exampleBoard.getBlock(new Coordinate(5, 5)).setPiece(new King(Color.BLACK));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(5, 5));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(5, 5)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 6)));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 6)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 4)));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 4)));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing black King normal movements.", resultingTree, actualTree);
    }

    /** Test the single King basic movements in the corner. */
    @Test
    public void testSimpleKingMovementsBlocked() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(0, 2)).setPiece(new King(Color.WHITE));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(0, 2));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(0, 2)));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 1)));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 3)));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white King normal movements in the corner.", resultingTree, actualTree);
        exampleBoard.getBlock(new Coordinate(7, 5)).setPiece(new King(Color.BLACK));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(7, 5));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(7, 5)));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 6)));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 4)));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing black King normal movements in the corner.", resultingTree, actualTree);
    }

    /** Test the single Man basic Eat. */
    @Test
    public void testSimpleManEat() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(2, 0));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(2, 0)));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 1)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 2), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white Man normal eat move.", resultingTree, actualTree);
        exampleBoard.getBlock(new Coordinate(5, 7)).setPiece(new Man(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(4, 6)).setPiece(new Man(Color.WHITE));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(5, 7));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(5, 7)));
        treeBuilder.addValue(new StepImpl(new Coordinate(3, 5), new Pair<>(new Coordinate(4, 6), new Man(Color.WHITE))));
        treeBuilder.addValue(new StepImpl(new Coordinate(6, 6)));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing black Man normal eat move.", resultingTree, actualTree);
    }

    /** Test the single King basic Eat. */
    @Test
    public void testSimpleKingEat() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(2, 2)).setPiece(new King(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(1, 3)).setPiece(new Man(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(1, 1)).setPiece(new Man(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(3, 3)).setPiece(new Man(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(2, 2));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(2, 2)));
        treeBuilder.addValue(new StepImpl(new Coordinate(0, 4), new Pair<>(new Coordinate(1, 3), new Man(Color.BLACK))));
        treeBuilder.addValue(new StepImpl(new Coordinate(0, 0), new Pair<>(new Coordinate(1, 1), new Man(Color.BLACK))));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 0), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 4), new Pair<>(new Coordinate(3, 3), new Man(Color.BLACK))));
        Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing all white King normal eat move.", resultingTree, actualTree);
        exampleBoard.reset();
        exampleBoard.getBlock(new Coordinate(2, 2)).setPiece(new King(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(1, 3)).setPiece(new Man(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(1, 1)).setPiece(new Man(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(3, 3)).setPiece(new Man(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.WHITE));
        resultingTree = calculator.calc(exampleBoard, new Coordinate(2, 2));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(2, 2)));
        treeBuilder.addValue(new StepImpl(new Coordinate(0, 4), new Pair<>(new Coordinate(1, 3), new Man(Color.WHITE))));
        treeBuilder.addValue(new StepImpl(new Coordinate(0, 0), new Pair<>(new Coordinate(1, 1), new Man(Color.WHITE))));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 0), new Pair<>(new Coordinate(3, 1), new Man(Color.WHITE))));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 4), new Pair<>(new Coordinate(3, 3), new Man(Color.WHITE))));
        actualTree = treeBuilder.returnTree();
        assertEquals("Testing all black King normal eat move.", resultingTree, actualTree);
    }

    /** Test the multiple Man Eat. */
    @Test
    public void testMultipleManEat() {
        final Chessboard exampleBoard = new ChessboardImpl();
        exampleBoard.getBlock(new Coordinate(2, 0)).setPiece(new Man(Color.WHITE));
        exampleBoard.getBlock(new Coordinate(3, 1)).setPiece(new Man(Color.BLACK));
        exampleBoard.getBlock(new Coordinate(3, 3)).setPiece(new Man(Color.BLACK));
        final Tree<Step> resultingTree = calculator.calc(exampleBoard, new Coordinate(2, 0));
        treeBuilder.startNewTree(new StepImpl(new Coordinate(2, 0)));
        treeBuilder.addValue(new StepImpl(new Coordinate(4, 2), new Pair<>(new Coordinate(3, 1), new Man(Color.BLACK))));
        treeBuilder.addValue(new StepImpl(new Coordinate(1, 1)));
        treeBuilder.goToNextChild();
        treeBuilder.addValue(new StepImpl(new Coordinate(2, 4), new Pair<>(new Coordinate(3, 3), new Man(Color.BLACK))));
        final Tree<Step> actualTree = treeBuilder.returnTree();
        assertEquals("Testing white Man multiple eat move.", resultingTree, actualTree);
    }

}
