package it.unibo.modularcheckers.checkers.model.engine;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.base.Optional;

import it.unibo.modularcheckers.model.Color;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.engine.AbstractStepTreeFilter;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.Tree;
import it.unibo.modularcheckers.model.piece.PieceType;

/**
 * Checkers implementation for AbstractStepTreeFilter.
 */
public class CheckersStepTreeFilter extends AbstractStepTreeFilter {

    // Coordinates
    private Map<Coordinate, Color> kings;

    /**
     * Sole constructor. Initialize Kings to HashSet.
     */
    public CheckersStepTreeFilter() {
        super();
        this.kings = new HashMap<>();
    }

    /**
     * {@inheritDoc}. In checkers, only the most convenient move is possible.
     */
    @Override
    protected void applyProperFilters() {
        calculateKingCoordinates();
        filterByHeight();
        filterByEat();
        leaveOnlyKingEaten();
        trimAllTreesByHeight();
        trimAllTreesByEat();
        leaveMajorKingEat();
    }

    /**
     * Removes all the trees that have a height < of the tree with max height. This
     * is the first filter.
     */
    private void filterByHeight() {
        if (getStepTrees().values().isEmpty()) {
            throw new IllegalStateException("No moves are possible anymore.");
        }
        final int maxHeight = getStepTrees().values().stream().mapToInt(t -> t.height()).max().getAsInt();
        setStepTrees(getStepTrees().entrySet().stream().filter(e -> e.getValue().height() >= maxHeight)
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));
    }

    /**
     * Leave only the steps where a piece can eat another one.
     */
    private void filterByEat() {
        final Predicate<Coordinate> predicate = k -> getStepTrees().get(k)
                .levelOfFirstAppearance(tree -> tree.getRoot().getDeadPiece().isPresent()).isPresent();
        if (getStepTrees().keySet().stream().anyMatch(predicate)) {
            setStepTrees(getStepTrees().keySet().stream().filter(predicate)
                    .collect(Collectors.toMap(k -> k, k -> getStepTrees().get(k))));
            // If at least a piece moving is a King execute filterByEatKing
            if (getStepTrees().keySet().stream().filter(c -> getBoard().getBlock(c).pieceExists())
                    .anyMatch(c -> getBoard().getBlock(c).getPiece().get().getType().equals(PieceType.KING))) {
                filterByEatKing();
            }
        }
    }

    /**
     * Leave only trees where the piece eating is a King. This method runs only if a
     * King can eat.
     */
    private void filterByEatKing() {
        final Predicate<Coordinate> predicate = c -> getBoard().getBlock(c).pieceExists()
                && getBoard().getBlock(c).getPiece().get().getType().equals(PieceType.KING);
        setStepTrees(getStepTrees().keySet().stream().filter(predicate)
                .collect(Collectors.toMap(c -> c, c -> getStepTrees().get(c))));
    }

    /**
     * Check if some trees contains a Step where a King is eaten. if so, delete all
     * the others.
     */
    private void leaveOnlyKingEaten() {
        final Predicate<Entry<Coordinate, Tree<Step>>> predicate = e -> e.getValue()
                .levelOfFirstAppearance(tree -> tree.getRoot().getDeadPiece().isPresent()
                        && tree.getRoot().getDeadPiece().get().getY().getType().equals(PieceType.KING))
                .isPresent();
        // If there are opposing Kings.
        if (kings.values().stream().anyMatch(c -> !c.equals(getActualTurn()))) {
            final Map<Coordinate, Tree<Step>> kingsEatean = getStepTrees().entrySet().stream().filter(predicate)
                    .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
            if (!kingsEatean.isEmpty()) {
                setStepTrees(kingsEatean);
            }
        }
    }

    /*************************/
    /*** TRIMMING METHODS ****/
    /*************************/

    /**
     * Delete the branches where pieces are not eaten if at least one trees eat a
     * piece.
     */
    private void trimAllTreesByEat() {
        // If at least a piece dies in a Tree
        if (getStepTrees().values().stream().flatMap(t -> t.getAllValues().stream())
                .filter(s -> s.getDeadPiece().isPresent()).findAny().isPresent()) {
            getStepTrees().entrySet().stream()
                    .forEach(e -> e.getValue().getChildren().removeIf(c -> !c.getRoot().getDeadPiece().isPresent()));
        }

    }

    /**
     * Delete all the branches shorter than maxHeight on every Tree.
     */
    private void trimAllTreesByHeight() {
        setStepTrees(getStepTrees().entrySet().stream().peek(e -> e.getValue().balanceToHeight(e.getValue().height()))
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));

    }

    /**
     * Trim the tree by leaving only branches where more Kings are eaten. The run
     * leaveOnlyFirstKing method if there are 2 branches or more.
     */
    private void leaveMajorKingEat() {
        // If at least a King is eaten
        if (getStepTrees().values().stream().flatMap(t -> t.getAllValues().stream())
                .filter(s -> s.getDeadPiece().isPresent())
                .anyMatch(s -> s.getDeadPiece().get().getY().getType().equals(PieceType.KING))) {
            // Calculate the number of kings in children

            final Predicate<Tree<Step>> predicate = t -> t.getRoot().getDeadPiece().get().getY().getType()
                    .equals(PieceType.KING);
            final int maxNumberOfKingsEaten = getStepTrees().values().stream().flatMap(t -> t.getChildren().stream())
                    .mapToInt(t -> t.numberOfNodesForCondition(predicate)).max().getAsInt();
            // Delete the branches where there are < Kings

            setStepTrees(getStepTrees().entrySet().stream()
                    .peek(e -> e.getValue().getChildren()
                            .removeIf(t -> t.numberOfNodesForCondition(predicate) < maxNumberOfKingsEaten))
                    .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));

            if (getStepTrees().values().stream().anyMatch(t -> t.getChildren().size() >= 2)) {
                leaveOnlyFirstKing();
            }
        }
    }

    private void leaveOnlyFirstKing() {
        final int minKingLevel = getStepTrees().values().stream().flatMap(t -> t.getChildren().stream())
                .mapToInt(t -> calculateFirstKingLevel(t).get()).min().getAsInt();

        setStepTrees(getStepTrees().entrySet().stream()
                .peek(e -> e.getValue().getChildren().removeIf(
                        t -> calculateFirstKingLevel(t).isPresent() && calculateFirstKingLevel(t).get() > minKingLevel))
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue())));

    }

    /*************************/
    /**** UTILITY METHODS ****/
    /*************************/

    /**
     * Place in the King set the coordinates of the kings.
     */
    private void calculateKingCoordinates() {
        this.kings = getBoard().getBlocks().keySet().stream().filter(c -> getBoard().getBlock(c).getPiece().isPresent())
                .filter(c -> getBoard().getBlock(c).getPiece().get().getType().equals(PieceType.KING))
                .collect(Collectors.toMap(c -> c, c -> getBoard().getBlock(c).getPiece().get().getColor()));
    }

    /** Calculate the level of the tree where the kings appear as deadPiece. */
    private Optional<Integer> calculateFirstKingLevel(final Tree<Step> tree) {
        return tree.levelOfFirstAppearance(
                t -> t.getRoot().getDeadPiece().isPresent() && isKing(t.getRoot().getDeadPiece().get().getX()));
    }

    private boolean isKing(final Coordinate coord) {
        return kings.containsKey(coord);
    }

}
