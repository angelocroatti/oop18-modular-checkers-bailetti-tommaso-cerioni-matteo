package it.unibo.modularcheckers.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.Color;

/**
 * Basic implementation of GameDataDeserializer.
 */
public final class GameDataDeserializer {

    private GameDataDeserializer() {

    }

    /**
     * Return the deserialized List of colors for Checkers and Chess.
     * 
     * @throws IOException when streams fail to open or close.
     * @return the deserialized list of colors.
     */
    @SuppressWarnings("unchecked")
    public static List<Color> deserializeColor() throws IOException {
        ObjectInputStream in = null;
        List<Color> colorList = null;
        try {
            in = new ObjectInputStream(ConfPathHelper.getColorsPath());
            colorList = (List<Color>) in.readObject();
        } catch (IOException ex) {
            System.out.println("IOException is caught: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" + " is caught");
        } catch (Exception ex) {
            System.out.println("URI Malformed: " + ex.getMessage());
        } finally {
            closeStreams(in);
        }
        return colorList;
    }

    /**
     * Return the deserialized Board configuration for Checkers Game.
     * 
     * @throws IOException when streams fail to open or close.
     * @return the deserialized Chessboard.
     */
    public static Chessboard deserializeCheckersBoard() throws IOException {
        ObjectInputStream in = null;
        Chessboard board = null;
        try {
            in = new ObjectInputStream(ConfPathHelper.getCheckersPath());
            board = (Chessboard) in.readObject();
        } catch (IOException ex) {
            System.out.println("IOException is caught: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException" + " is caught");
        } catch (Exception ex) {
            System.out.println("URI Malformed: " + ex.getMessage());
        } finally {
            closeStreams(in);
        }
        return board;
    }

    private static void closeStreams(final ObjectInputStream in) throws IOException {
        in.close();
    }

}
