package it.unibo.modularcheckers.util;

import java.io.File;

/**
 * Assures the computer has every folder it may need.
 */
public final class LocalFilesUtils {

    /**
     * Base directory in $HOME folder.
     */
    public static final String BASE_DIR = ".modularCheckers";

    /**
     * History directory.
     */
    public static final String HISTORY_DIR = "history";

    private LocalFilesUtils() {

    }

    /**
     * Checks for the home directory structure, tries to create it if it's missing.
     */
    public static void checkDirStructure() {
        final File home = new File(System.getProperty("user.home") + File.separator + BASE_DIR);
        if (!home.exists()) {
            home.mkdir();
            final File history = new File(System.getProperty("user.home") + File.separator + BASE_DIR
                    + File.separator + HISTORY_DIR);
            if (!history.exists()) {
                history.mkdir();
            }
        }
    }

    /**
     * Returns the full path of the history folder.
     *
     * @return String of the path.
     */
    public static String getHistoryPath() {
        return System.getProperty("user.home") + File.separator + BASE_DIR + File.separator + HISTORY_DIR;
    }
}
