package it.unibo.modularcheckers.model;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import it.unibo.modularcheckers.model.move.Step;

/**
 * Random player. The move to execute is chosen randomly.
 */
public class RandomPlayer extends AbstractPlayer implements BotPlayer {

    /**
     * Superclass constructor.
     * 
     * @param playerName the player name.
     */
    public RandomPlayer(final String playerName) {
        super(playerName);
    }

    /**
     * The piece is chosen randomly. {@inheritDoc}
     *
     */
    @Override
    public Optional<Coordinate> selectedPiece(final Chessboard chessboard, final Set<Coordinate> movablePieces) {
        final int index = new Random().nextInt(movablePieces.size());
        final Iterator<Coordinate> iter = movablePieces.iterator();
        for (int i = 0; i < index; i++) {
            iter.next();
        }
        return Optional.of(iter.next());
    }

    /**
     * Return a random decision. There will always be a child. When the move is
     * created the check is on the the children of the Tree returned. No more checks
     * are required here. {@inheritDoc}.
     */
    @Override
    public Coordinate chooseStep(final Chessboard chessboard, final List<Step> steps) {
        return steps.get(new Random().nextInt(steps.size())).getCoordinate();
    }

}
