package it.unibo.modularcheckers.model.move;

import java.util.List;
import java.util.function.Predicate;

import com.google.common.base.Optional;

/**
 * Represents a generic tree.
 * 
 * @param <X> the generic class of the nodes.
 */
public interface Tree<X> {

    /**
     * Get the root of the tree.
     * 
     * @return the root of the tree.
     */
    X getRoot();

    /**
     * Get all the children of the tree.
     * 
     * @return a list containing the children of the tree.
     */
    List<Tree<X>> getChildren();

    /**
     * @return a List containing all the nodes.
     */
    List<X> getAllValues();

    /**
     * @return a List containing all the nodes as Tree.
     */
    List<Tree<X>> getAllNodes();

    /**
     * Return all the children of the first level of the tree.
     * 
     * @return a list containing only the first level of the tree.
     */
    List<X> getFirstChildren();

    /**
     * 
     * @return the height of the Tree.
     */
    int height();

    /**
     * Trim a tree, by leaving only branches of at least height maxHeight.
     * 
     * @param maxHeight height of the final tree.
     * @return true if remains at least one branch of height maxHeight or greater.
     */
    boolean balanceToHeight(int maxHeight);

    /**
     * Find the level of the Tree where a node respect a certain condition.
     * 
     * @param condition the condition to check on a Node.
     * @return the minimum level the Tree when the condition passed is found.
     */
    Optional<Integer> levelOfFirstAppearance(Predicate<Tree<X>> condition);

    /**
     * Return the number of nodes that respects the condition in the Tree. Root is
     * included.
     * 
     * @param condition the condition to check.
     * @return the number of nodes that respects the condition.
     */
    int numberOfNodesForCondition(Predicate<Tree<X>> condition);

}
