package it.unibo.modularcheckers.model.engine;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Optional;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.model.Player;
import it.unibo.modularcheckers.model.move.Step;
import it.unibo.modularcheckers.model.move.Tree;

/**
 * Engine is an Interface that model a sort of "referee" for every game played.
 * The rule-set is chosen based on the game, and any move can be executed only
 * if the Engine decide it is legal.
 */
public interface Engine {

    /**
     * This method is called to return the Board status in a certain situation.
     *
     * @return the Board status.
     */
    Chessboard getLogicBoard();

    /**
     * This method is called to return the player that needs to choose the move to
     * do.
     *
     * @return the Player whose turn is now.
     */
    Player getActualPlayer();

    /**
     * @return the winners of the game. If there are more Players in the set, the
     *         game is TIED.
     */
    Set<Player> getWinners();

    /**
     * @return the status of the game.
     */
    GameStatus getStatus();

    /**
     * @return true if the move is still in progress.
     */
    boolean isMoveInProgress();

    /**
     * Calculate the Tree of Steps for every piece on the board of the actualTurn
     * color.
     *
     * @return A map where every piece in that coordinate have its relative
     *         Step-Tree.
     */
    Map<Coordinate, Tree<Step>> calculateAllSteps();

    /**
     * Return the specific Tree of steps of a Piece.
     *
     * @param coordSelected The Coordinate of the piece chosen.
     * @return An Optional tree containing the steps of the piece chosen. Optional
     *         empty if not present.
     */
    Optional<Tree<Step>> getStepTreeFromCoord(Coordinate coordSelected);

    /**
     * @return A set containing the coordinates of all the pieces that can move in
     *         the turn.
     */
    Set<Coordinate> getCoordsOfMovablePieces();

    /**
     * @return the coordinate of the last piece that moved, if present.
     */
    Optional<Coordinate> getCoordinateOfLastPieceMoved();

    /**
     * Execute the steps chosen by the player. (Method ChooseStep)
     *
     * @param steps X: the step containing the coordinate where the piece is. Y: The
     *              step containing the coordinate where to move.
     * @return A list of the changes happened on the chessboard.
     */
    List<Pair<Coordinate, Block>> executeStep(Pair<Step, Step> steps);

    /**
     * Called when the Tree of step is over. Adds the move to the History, skips to
     * the next Turn and calculates all the new StepTrees.
     */
    void moveFinished();

    /**
     * Start the game.
     */
    void start();

    /**
     * Removes all the pieces of the player that decide to surrender.
     */
    void surrender();

    /**
     * Saves the match finished to a file.
     */
    void saveMatchToFile();
}
