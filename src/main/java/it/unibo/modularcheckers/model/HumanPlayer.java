package it.unibo.modularcheckers.model;

/**
 * Implementation of Player. He needs to interact with the view to choose the
 * move.
 */
public class HumanPlayer extends AbstractPlayer {

    /**
     * Superclass constructor.
     * 
     * @param playerName the player name.
     */
    public HumanPlayer(final String playerName) {
        super(playerName);
    }
}
