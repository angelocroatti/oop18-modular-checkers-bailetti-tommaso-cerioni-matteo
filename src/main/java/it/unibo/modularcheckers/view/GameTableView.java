package it.unibo.modularcheckers.view;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;

import java.util.List;

/**
 * Interface to interact with view.
 */
public interface GameTableView {

    /**
     * Set the next available moves.
     *
     * @param nextSteps next steps possible.
     */
    void setNextMoves(List<Coordinate> nextSteps);

    /**
     * Applies the changes from the chessboard to the view.
     *
     * @param changes list of block to be changed
     */
    void applyChanges(List<Pair<Coordinate, Block>> changes);

    /**
     * Show a dialog to let the user surrender.
     * @param message to be displayed
     * @param title title of the dialog
     * @return JOptionPane yes/no value
     */
    int surrenderDialog(String message, String title);

    /**
     * Shows a dialog announcing the winner!
     * @param message to be displayed
     */
    void winnerDialog(String message);
}
