package it.unibo.modularcheckers.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import it.unibo.modularcheckers.model.Block;
import it.unibo.modularcheckers.model.Chessboard;
import it.unibo.modularcheckers.model.Coordinate;
import it.unibo.modularcheckers.model.Pair;
import it.unibo.modularcheckers.util.GameDataDeserializer;
import it.unibo.modularcheckers.view.components.BlockComponent;
import it.unibo.modularcheckers.view.components.ChessBoardComponent;
import it.unibo.modularcheckers.view.observers.GameTableViewObservable;

/**
 * View defining the base of a chess board.
 */
public class GameTableViewImpl extends View implements GameTableView {

    private static final long serialVersionUID = -1930603709528904000L;
    private final Map<JButton, Pair<Coordinate, Block>> map;

    /**
     * Initialize the GameTableViewImpl with the previously serialized Class.
     * 
     * @param observable observer of the view
     * @throws IOException in case deserialization fails
     */
    public GameTableViewImpl(final GameTableViewObservable observable) throws IOException {
        super();
        final Chessboard chessboard = GameDataDeserializer.deserializeCheckersBoard();

        map = new HashMap<>();
        final JPanel panel = new JPanel(new BorderLayout(3, 3));
        final JPanel container = new JPanel(new GridBagLayout());
        final JPanel grid = new ChessBoardComponent(chessboard);

        final JToolBar toolBar = new JToolBar();
        final JButton btnSurrender = new JButton("Surrender");
        btnSurrender.addActionListener(l -> observable.surrender());
        toolBar.add(btnSurrender);
        toolBar.setFloatable(false);
        panel.add(toolBar, BorderLayout.NORTH);

        for (int j = chessboard.getSize().getX() - 1; j >= 0; j--) {
            for (int i = 0; i < chessboard.getSize().getY(); i++) {
                final BlockComponent button = new BlockComponent();
                if ((i % 2 == 0 && j % 2 == 1) || (i % 2 == 1 && j % 2 == 0)) {
                    button.setBackground(Color.WHITE);
                } else {
                    button.setBackground(Color.BLACK);
                }
                button.addActionListener(l -> {
                    if (button.isNextMove()) {
                        observable.makeStepChosen(map.get(button).getX());
                    } else {
                        observable.selectPiece(Optional
                                .of(new Coordinate(map.get(button).getX().getX(), map.get(button).getX().getY())));
                    }
                });
                button.setBlock(chessboard.getBlock(new Coordinate(i, j)));
                map.put(button, new Pair<>(new Coordinate(i, j), chessboard.getBlock(new Coordinate(i, j))));
                grid.add(button);
            }
        }

        container.add(grid);
        panel.add(container);
        this.add(panel);
        this.setFullScreen();
        this.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setNextMoves(final List<Coordinate> nextSteps) {
        map.entrySet().stream().filter(entry -> ((BlockComponent) entry.getKey()).isNextMove())
                .forEach(entry -> ((BlockComponent) entry.getKey()).setNextMove(false));
        nextSteps.forEach(
                coordinate -> map.entrySet().stream().filter(entry -> entry.getValue().getX().equals(coordinate))
                        .forEach(entry -> ((BlockComponent) entry.getKey()).setNextMove(true)));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyChanges(final List<Pair<Coordinate, Block>> changes) {
        map.entrySet().stream().filter(entry -> ((BlockComponent) entry.getKey()).isNextMove())
                .forEach(entry -> ((BlockComponent) entry.getKey()).setNextMove(false));
        changes.forEach(change -> map.entrySet().stream().filter(entry -> entry.getValue().getX().equals(change.getX()))
                .forEach(val -> ((BlockComponent) val.getKey()).setBlock(change.getY())));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int surrenderDialog(final String message, final String title) {
        return JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void winnerDialog(final String message) {
        JOptionPane.showMessageDialog(null, message);
        this.dispose();
    }
}
