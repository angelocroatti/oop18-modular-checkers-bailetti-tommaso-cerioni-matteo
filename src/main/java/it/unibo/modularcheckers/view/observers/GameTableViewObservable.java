package it.unibo.modularcheckers.view.observers;

import java.util.Optional;

import it.unibo.modularcheckers.model.Coordinate;

/**
 * Lets the view to be controlled by the the GameLoop.
 */
public interface GameTableViewObservable {

    /**
     * When the player decide to surrender.
     */
    void surrender();

    /**
     * Select the Piece to move.
     *
     * @param pieceSelected the piece chosen on the view. Empty if none is selected.
     */
    void selectPiece(Optional<Coordinate> pieceSelected);

    /**
     * Make the step chosen for the selected piece.
     *
     * @param whereToMove the coordinate where to move.
     */
    void makeStepChosen(Coordinate whereToMove);
}
